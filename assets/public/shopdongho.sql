-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 31, 2019 at 10:46 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopdongho`
--

-- --------------------------------------------------------

--
-- Table structure for table `binhluan`
--

DROP TABLE IF EXISTS `binhluan`;
CREATE TABLE IF NOT EXISTS `binhluan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenkhachhang` varchar(200) NOT NULL,
  `noidung` varchar(200) NOT NULL,
  `idsanpham` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `binhluan`
--

INSERT INTO `binhluan` (`id`, `tenkhachhang`, `noidung`, `idsanpham`) VALUES
(2, 'tung', 'con cho', 1),
(3, 'tung', 'con cho', 0),
(4, 'tung', 'con cho', 0),
(5, 'tung', 'lecamtung', 4),
(6, 'tung', 'lecamtung123456', 4),
(7, 'tungdeptrai', 'cua hang vip', 3),
(8, 'tungdeptrai', 'cua hang vip', 3),
(9, 'tuan', '096544', 3),
(10, 'tuan', 'tao da mua cai nay', 4),
(11, 'tuan', 'tao moi mua rolex ne', 3),
(12, 'tien.', 'tao da mua rolex', 0),
(13, 'ff', 'ffwfwfw', 0),
(14, 'fefgergergerg', 'gwgg', 0),
(15, 'hdhdhdhfd', 'dhdfhd', 0),
(16, 'tung dep trai', 'da mua cai nay', 3),
(17, 'tung1112', '123456789', 3),
(18, 'tung 6:06', 'binh luan', 3),
(19, 'tao', 'binh luan', 3),
(20, '1235', 'dong ho xin', 2),
(21, 'thuan', 'haha', 4),
(22, 'Si', 'ok', 4),
(23, 'dhdh', 'dhdh', 2),
(24, 'dhdh', 'dhdh', 2),
(25, 'gh', 'cjg', 3),
(26, 'gh', 'cjg', 3),
(27, 'fg', 'ff', 2),
(28, 'hv', 'cứ dbh', 2),
(29, 'hv', 'cứ dbh', 2),
(30, 'cf', 'ggy', 2),
(31, 'gg', 'gg', 2),
(32, 'yuiu', 'hhh', 4),
(33, 'canh sat', 'lam an co tam', 4),
(34, 'le tung', 'dong ho dep dep chat luong', 4),
(35, 'le tung', 'dong ho dep dep chat luong', 4),
(36, 'le tung', 'dong ho dep dep chat luong', 4),
(37, 'tien', 'san pham dep hay', 4);

-- --------------------------------------------------------

--
-- Table structure for table `chitietdonhang`
--

DROP TABLE IF EXISTS `chitietdonhang`;
CREATE TABLE IF NOT EXISTS `chitietdonhang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `madonhang` int(11) NOT NULL,
  `masanpham` int(11) NOT NULL,
  `tensanpham` varchar(10000) NOT NULL,
  `giasanpham` int(11) NOT NULL,
  `soluongsanpham` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chitietdonhang`
--

INSERT INTO `chitietdonhang` (`id`, `madonhang`, `masanpham`, `tensanpham`, `giasanpham`, `soluongsanpham`) VALUES
(54, 44, 4, 'rolex 2019', 290000, 1),
(55, 44, 1, 'G-shock 1997', 300, 1),
(56, 45, 4, 'rolex 2019', 290000, 1),
(57, 50, 5, 'rolex hihi', 6451460, 5),
(58, 50, 2, 'G-shock 1998', 350, 1),
(59, 51, 4, 'rolex 2019', 3190000, 11);

-- --------------------------------------------------------

--
-- Table structure for table `donhang`
--

DROP TABLE IF EXISTS `donhang`;
CREATE TABLE IF NOT EXISTS `donhang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenkhachhang` varchar(200) NOT NULL,
  `sodienthoai` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `donhang`
--

INSERT INTO `donhang` (`id`, `tenkhachhang`, `sodienthoai`, `email`) VALUES
(44, 'le cam tung', 961325198, 'lecamtung@gmail.com'),
(45, 'tung dep trai', 1666167445, 'lecamtung@gmail.com'),
(46, 'tung dep trai', 1666167445, 'lecamtung@gmail.com'),
(47, 'tung dep trai', 1666167445, 'lecamtung@gmail.com'),
(48, 'tung dep trai', 1666167445, 'lecamtung@gmail.com'),
(49, 'tung dep trai', 1666167445, 'lecamtung@gmail.com'),
(50, 'tung123456', 96132517, 'lecamtung@gmail.com'),
(51, 'tung', 1677933189, 'lecamtung@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `loai_san_pham`
--

DROP TABLE IF EXISTS `loai_san_pham`;
CREATE TABLE IF NOT EXISTS `loai_san_pham` (
  `id_loai` int(11) NOT NULL AUTO_INCREMENT,
  `ten_loai` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Chưa cập nhật' COMMENT 'Tên loại sản phẩm',
  `hinhanhloaisanpham` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_loai`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Loại sản phẩm';

--
-- Dumping data for table `loai_san_pham`
--

INSERT INTO `loai_san_pham` (`id_loai`, `ten_loai`, `hinhanhloaisanpham`) VALUES
(4, 'Original Swiss', ''),
(5, 'Memorigin', ''),
(6, 'Đồng hồ Citizen', ''),
(7, 'Diamond D', '');

-- --------------------------------------------------------

--
-- Table structure for table `san_pham`
--

DROP TABLE IF EXISTS `san_pham`;
CREATE TABLE IF NOT EXISTS `san_pham` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_loai` int(11) NOT NULL,
  `ten_san_pham` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Chưa cập nhật',
  `mo_ta` text COLLATE utf8_unicode_ci NOT NULL,
  `gia` int(15) NOT NULL,
  `hinh_anh` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Sản phẩm';

--
-- Dumping data for table `san_pham`
--

INSERT INTO `san_pham` (`id`, `id_loai`, `ten_san_pham`, `mo_ta`, `gia`, `hinh_anh`) VALUES
(9, 4, 'Origin', 'Origin Đồng hồ cao cấp', 400000, 'originalswiss3.jpg'),
(10, 6, 'Citizen plus 3', 'Đồng hồ citizen chất lượng cao', 3000000, 'citizen3.jpg'),
(11, 7, 'Diamond D 01', '', 700000, 'diamondd4.jpg'),
(12, 7, 'Diamond D02', 'Diamond D cao cấp doanh nhân', 9000000, 'diamondd.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Chưa đặt tên',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Account user';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `role`) VALUES
(1, 'Huỳnh Minh Tiến', 'tienhuynh@gmail.com', '9e7f9beb5041eae804813098c86b17d2', 1),
(2, 'Nhân Viên 01', 'nhanvien@gmail.com', '2a2fa4fe2fa737f129ef2d127b861b7e', 0),
(4, 'Nhân Viên 03', 'nhanvien03@gmail.com', '2a2fa4fe2fa737f129ef2d127b861b7e', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
