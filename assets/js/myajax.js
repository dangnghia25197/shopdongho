$(function() {
    // Document is ready
    'use strict'

    $('.pagination.justify-content-center .page-item a').addClass('page-link');




    //        $('#create_san_pham').submit(function(e){
    //          e.preventDefault(); 
    //          $.ajax({
    //                url: BASE_URL+'admin/create_san_pham',
    //                dataType:'json',
    //                 data: new FormData(this),
    //                  processData: false,
    //                  contentType: false,
    //                  type: 'POST',
    //                  success: function(data){
    //                    if(data.error == true){
    //                        swal({
    //                              title: "Lỗi!",
    //                              text: ''+data.messenge,
    //                              imageUrl: '../assets/images/icon/error_img.png'
    //                            });
    //                    }
    //                    if(data.error == false){
    //                        $('#create_san_pham')[0].reset(); //reset form cho lan submit sau
    //                        $('#modalThemSanPham').modal('hide');
    //                        swal(data.messenge,"", "success");
    //                    }
    //                  }
    //             });
    //        });



    //Preview image
    $('.hinh_anh').change(function() {
        var reader = new FileReader();
        var $preview = $(this).parents('.card-body').find('.image_preview');
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            $preview.attr('src', e.target.result);
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });



    //Them method validation for selected tag
    jQuery.validator.addMethod(
        "notEqualTo",
        function(elementValue, element, param) {
            return elementValue != param;
        },
        "Vui lòng chọn một loại sản phẩm"
    );
    jQuery.validator.addMethod(
        "notEqualTo_role",
        function(elementValue, element, param) {
            return elementValue != param;
        },
        "Vui lòng chọn một vai trò"
    );


    $("#create_san_pham").validate({
        rules: {
            // simple rule, converted to {required:true}
            ten_san_pham: "required",
            id_loai: {
                required: true,
                notEqualTo: 0
            },
            gia: {
                required: true,
                number: true,
                min: 50000
            },
            hinh_anh: {
                required: true
            }
        },
        submitHandler: function(form) {

            $.ajax({
                url: BASE_URL + 'admin/create_san_pham',
                dataType: 'json',
                data: new FormData(form),
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data) {
                    if (data.error == true) {
                        swal({
                            title: "Lỗi!",
                            text: '' + data.messenge,
                            imageUrl: '../assets/images/icon/error_img.png'
                        });
                    }
                    if (data.error == false) {
                        $('#create_san_pham')[0].reset(); //reset form cho lan submit sau
                        $('#modalThemSanPham').modal('hide');
                        swal(data.messenge, "", "success");

                        setTimeout(function() { location.reload(true); }, 1000);

                    }
                }
            });


        },
        messages: {
            ten_san_pham: "Trường này không được để trống!",
            gia: {
                required: "Trường này không được để trống!",
                min: jQuery.validator.format("vui lòng nhập giá tiền >= {0}")
            },
            hinh_anh: { required: 'Vui lòng chọn ảnh upload!' }

        },
        errorClass: "is-invalid",
        validClass: "is-valid",
        errorPlacement: function(label, element) {
            label.addClass('invalid-feedback');
            label.insertAfter(element);
        }


    });




    function updatesanpham(id) {
        let $url = BASE_URL + "admin/get_san_pham_id/" + id;
        $('#update_san_pham')[0].reset(); //reset form cho lan submit sau
        $.ajax({
            url: $url,
            dataType: 'json',
            type: 'GET',
            success: function(data) {
                $('#update_san_pham')[0].ten_san_pham.value = data.ten_san_pham;
                $('#update_san_pham')[0].id_loai.value = data.id_loai;
                $('#update_san_pham')[0].gia.value = data.gia;
                $('#update_san_pham')[0].mo_ta.value = data.mo_ta;
                $('#update_san_pham')[0].image_preview.src = BASE_URL + 'assets/images/' + data.hinh_anh;
            },
            error: function(e) {

                swal({
                    title: "Lỗi!",
                    text: '' + e,
                    imageUrl: '../assets/images/icon/error_img.png'
                });

            }
        });

        //validate
        $("#update_san_pham").validate({
            rules: {
                // simple rule, converted to {required:true}
                ten_san_pham: "required",
                id_loai: {
                    required: true,
                    notEqualTo: 0
                },
                gia: {
                    required: true,
                    number: true,
                    min: 50000
                },
                hinh_anh: {
                    required: false
                }
            },
            submitHandler: function(form) {

                $.ajax({
                    url: BASE_URL + "admin/update_san_pham/" + id,
                    dataType: 'json',
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function(data) {
                        if (data.error == true) {
                            swal({
                                title: "Lỗi!",
                                text: '' + data.messenge,
                                imageUrl: '../assets/images/icon/error_img.png'
                            });
                        }
                        if (data.error == false) {
                            $('#update_san_pham')[0].reset(); //reset form cho lan submit sau
                            $('#modalSuaSanPham').modal('hide');
                            swal(data.messenge, "", "success");
                            setTimeout(function() { location.reload(true); }, 1000);
                        }
                    }
                });


            },
            messages: {
                ten_san_pham: "Trường này không được để trống!",
                gia: {
                    required: "Trường này không được để trống!",
                    min: jQuery.validator.format("vui lòng nhập giá tiền >= {0}")
                }
            },
            errorClass: "is-invalid",
            validClass: "is-valid",
            errorPlacement: function(label, element) {
                label.addClass('invalid-feedback');
                label.insertAfter(element);
            }


        }); //end validate
    }

    function updateloaisanpham(id) {
        let $url = BASE_URL + "admin/get_loai_san_pham_id/" + id;
        $('#update_loai_san_pham')[0].reset(); //reset form cho lan submit sau
        $.ajax({
            url: $url,
            dataType: 'json',
            type: 'GET',
            success: function(data) {
                $('#update_loai_san_pham')[0].ten_loai.value = data.ten_loai;

            },
            error: function(e) {

                swal({
                    title: "Lỗi!",
                    text: '' + e,
                    imageUrl: '../assets/images/icon/error_img.png'
                });

            }
        });

        //validate
        $("#update_loai_san_pham").validate({
            rules: {
                ten_loai: "required"
            },
            submitHandler: function(form) {

                $.ajax({
                    url: BASE_URL + "admin/update_loai_san_pham/" + id,
                    dataType: 'json',
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function(data) {
                        if (data.error == true) {
                            swal({
                                title: "Lỗi!",
                                text: '' + data.messenge,
                                imageUrl: '../assets/images/icon/error_img.png'
                            });
                        }
                        if (data.error == false) {
                            $('#update_loai_san_pham')[0].reset(); //reset form cho lan submit sau
                            $('#modalSuaLoaiSanPham').modal('hide');
                            swal(data.messenge, "", "success");
                            setTimeout(function() { location.reload(true); }, 1000);
                        }
                    }
                });


            },
            messages: {
                ten_loai: "Trường này không được để trống!"
            },
            errorClass: "is-invalid",
            validClass: "is-valid",
            errorPlacement: function(label, element) {
                label.addClass('invalid-feedback');
                label.insertAfter(element);
            }


        }); //end validate
    }

    function updateuser(id) {
        let $url = BASE_URL + "admin/get_user_with_id/" + id;
        $('#update_user_form')[0].reset(); //reset form cho lan submit sau
        $.ajax({
            url: $url,
            dataType: 'json',
            type: 'GET',
            success: function(data) {
                $('#update_user_form')[0].username.value = data.username;
                $('#update_user_form')[0].role.value = data.role;
                $('#update_user_form')[0].email.value = data.email;
                $('#update_user_form')[0].password.value = data.password;
            },
            error: function(e) {
                swal({
                    title: "Lỗi!",
                    text: '' + e,
                    imageUrl: '../assets/images/icon/error_img.png'
                });

            }
        });
        //validate
        $("#update_user_form").validate({
            rules: {
                username: "required",
                role: {
                    required: true,
                    notEqualTo_role: -1
                },
                email: "required",
                password: {
                    required: true,
                    minlength: 6
                }
            },
            submitHandler: function(form) {

                $.ajax({
                    url: BASE_URL + "admin/update_user/" + id,
                    dataType: 'json',
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function(data) {
                        if (data.error == true) {
                            swal({
                                title: "Lỗi!",
                                text: '' + data.messenge,
                                imageUrl: '../assets/images/icon/error_img.png'
                            });
                        }
                        if (data.error == false) {
                            $('#update_user_form')[0].reset(); //reset form cho lan submit sau
                            $('#modalSuaUser').modal('hide');
                            swal(data.messenge, "", "success");
                            setTimeout(function() { location.reload(true); }, 1000);
                        }
                    }
                });


            },
            messages: {
                username: "Trường này không được để trống!",
                email: "Trường này không được để trống!",
                password: {
                    required: "Trường này không được để trống!",
                    minlength: jQuery.validator.format("Đồ dài password phải >= {0}")
                }
            },
            errorClass: "is-invalid",
            validClass: "is-valid",
            errorPlacement: function(label, element) {
                label.addClass('invalid-feedback');
                label.insertAfter(element);
            }


        }); //end validate
    }




    //onclick on buton sua and load data
    $('.item').click(function() {
        let id = $(this).data("idsanpham");
        let target = $(this).data('target');

        if (target == "#modalSuaSanPham") {
            updatesanpham(id);
        }
        if (target == "#modalSuaLoaiSanPham") {
            updateloaisanpham(id);
        }
        if (target == "#modalSuaUser") {
            updateuser(id);
        }


    }); // end onclick


    function DeleteSanPham(id) {
        let $url = BASE_URL + "admin/delete_san_pham/" + id;
        swal({
                title: "Bạn chắc chứ?",
                text: "Dữ liệu của bạn có thể sẽ bị mất! \n\n",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Vâng, Xóa!",
                cancelButtonText: "Hủy",
                closeOnConfirm: false
            },
            function() {
                //call api to delete san pham
                $.ajax({
                    url: $url,
                    dataType: 'json',
                    type: 'GET',
                    success: function(data) {
                        if (data != 0) {
                            swal("\n Đã xóa!", "Sản phẩm đã được xóa. \n\n", "success");
                            setTimeout(function() { location.reload(true); }, 1000);
                        } else {
                            swal({
                                title: "Lỗi !",
                                text: '' + e,
                                imageUrl: '../assets/images/icon/error_img.png'
                            });
                        }
                    },
                    error: function(e) {
                        swal({
                            title: "Lỗi!",
                            text: '' + e,
                            imageUrl: '../assets/images/icon/error_img.png'
                        });

                    }
                });
            }); // end swal
    }

    function DeleteLoaiSanPham(id) {
        let $url = BASE_URL + "admin/delete_loai_san_pham/" + id;
        swal({
                title: "Bạn chắc chứ?",
                text: "Dữ liệu của bạn có thể sẽ bị mất! \n\n",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Vâng, Xóa!",
                cancelButtonText: "Hủy",
                closeOnConfirm: false
            },
            function() {
                //call api to delete san pham
                $.ajax({
                    url: $url,
                    dataType: 'json',
                    type: 'GET',
                    success: function(data) {
                        if (data != 0) {
                            swal("\n Đã xóa!", "Loại sản phẩm đã được xóa. \n\n", "success");
                            setTimeout(function() { location.reload(true); }, 1000);
                        } else {
                            swal({
                                title: "Lỗi !",
                                text: '' + 'Lỗi ở server',
                                imageUrl: '../assets/images/icon/error_img.png'
                            });
                        }
                    },
                    error: function(e) {
                        swal({
                            title: "Lỗi!",
                            text: '' + e,
                            imageUrl: '../assets/images/icon/error_img.png'
                        });

                    }
                });
            }); // end swal
    }

    function DeleteUser(id) {
        let $url = BASE_URL + "admin/delete_user/" + id;
        swal({
                title: "Bạn chắc chứ?",
                text: "Dữ liệu của bạn có thể sẽ bị mất! \n\n",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Vâng, Xóa!",
                cancelButtonText: "Hủy",
                closeOnConfirm: false
            },
            function() {
                //call api to delete san pham
                $.ajax({
                    url: $url,
                    dataType: 'json',
                    type: 'GET',
                    success: function(data) {
                        if (data != 0) {
                            swal("\n Đã xóa!", "User đã được xóa. \n\n", "success");
                            setTimeout(function() { location.reload(true); }, 1000);
                        } else {
                            swal({
                                title: "Lỗi !",
                                text: '' + 'Lỗi ở server',
                                imageUrl: '../assets/images/icon/error_img.png'
                            });
                        }
                    },
                    error: function(e) {
                        swal({
                            title: "Lỗi!",
                            text: '' + e,
                            imageUrl: '../assets/images/icon/error_img.png'
                        });

                    }
                });
            }); // end swal
    }

    $('.item.delete_sanpham').click(function() {
        let target = $($(this).parent().children()[0]).data('target');
        let id = $(this).data("idsanpham");

        console.log(target, id);
        if (target == "#modalSuaSanPham") {
            DeleteSanPham(id);
        }

        if (target == "#modalSuaLoaiSanPham") {
            DeleteLoaiSanPham(id);
        }

        if (target == "#modalSuaUser") {
            DeleteUser(id);
        }
    }); //end click




    //LOAI SAN PHAM
    $("#create_loai_san_pham").validate({
        rules: {
            ten_loai: "required",
        },
        submitHandler: function(form) {
            $.ajax({
                url: BASE_URL + 'admin/create_loai_san_pham',
                dataType: 'json',
                data: new FormData(form),
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data) {
                    if (data.error == true) {
                        swal({
                            title: "Lỗi!",
                            text: '' + data.messenge,
                            imageUrl: '../assets/images/icon/error_img.png'
                        });
                    }
                    if (data.error == false) {
                        $('#create_loai_san_pham')[0].reset(); //reset form cho lan submit sau
                        $('#modalThemLoaiSanPham').modal('hide');
                        swal(data.messenge, "", "success");
                        setTimeout(function() { location.reload(true); }, 1000);
                    }
                }
            });
        },
        messages: {
            ten_loai: "Trường này không được để trống!",

        },
        errorClass: "is-invalid",
        validClass: "is-valid",
        errorPlacement: function(label, element) {
            label.addClass('invalid-feedback');
            label.insertAfter(element);
        }
    });


    //USER
    $("#create_user_form").validate({
        rules: {
            username: "required",
            role: {
                required: true,
                notEqualTo_role: -1
            },
            email: "required",
            password: {
                required: true,
                minlength: 6
            }
        },
        submitHandler: function(form) {
            $.ajax({
                url: BASE_URL + 'admin/create_user',
                dataType: 'json',
                data: new FormData(form),
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data) {
                    if (data.error == true) {
                        swal({
                            title: "Lỗi!",
                            text: '' + data.messenge,
                            imageUrl: '../assets/images/icon/error_img.png'
                        });
                    }
                    if (data.error == false) {
                        $('#create_user_form')[0].reset(); //reset form cho lan submit sau
                        $('#modalThemUser').modal('hide');
                        swal(data.messenge, "", "success");
                        setTimeout(function() { location.reload(true); }, 1000);
                    }
                }
            });
        },
        messages: {
            username: "Trường này không được để trống!",
            email: "Trường này không được để trống!",
            password: {
                required: "Trường này không được để trống!",
                minlength: jQuery.validator.format("Đồ dài password phải >= {0}")
            }
        },
        errorClass: "is-invalid",
        validClass: "is-valid",
        errorPlacement: function(label, element) {
            label.addClass('invalid-feedback');
            label.insertAfter(element);
        }
    });

});