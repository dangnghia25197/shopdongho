<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class UserAuthentication extends CI_Controller {

    public function __construct() {
        parent::__construct(); 
        // Load form helper library
        $this->load->helper('form');
        // Load form validation library
        $this->load->library('form_validation');
        // Load session library
        $this->load->library('session');
        // Lod database
        $this->load->model('User_model');
    }

    public function process_login(){
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_message('required', '%s không được để trống.');
        if ($this->form_validation->run() == FALSE) {
           $data['vali_error'] = validation_errors();
            $data['form'] = 'admin/login/login_form';
            $this->load->view('admin/login/main_login',$data);
        }
        else
        {
            if($this->User_model->login() == true){
                $info = $this->User_model->read_user_information($this->input->post('email'));
                if($info != FALSE){
                    $session_data = array(
                        'username' => $info['username'],
                        'email' => $info['email'],
                        'role' => $info['role']
                    );
                    $this->session->set_userdata('logged_in', $session_data);
                    redirect('/admin/get_san_pham');
                }
            }
            else{
                $data['mess_error'] = "Email hoặc Passord không đúng!";
                $data['form'] = 'admin/login/login_form';
                $this->load->view('admin/login/main_login', $data);
            }
        }
    }
    

    
}
