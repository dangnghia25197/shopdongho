<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Admin extends CI_Controller {


    //Login
	public function index()
	{   
        if (isset($this->session->userdata['logged_in'])) {
            redirect('/admin/get_san_pham');
        }

        $data['form'] = 'admin/login/login_form';
        $this->load->view('admin/login/main_login',$data);
    }


    // Logout from admin page
    public function logout() {

        // Removing session data
        $session_data = array(
            'username' =>'',
            'email' => '',
            'role' => ''
        );
        $this->session->unset_userdata('logged_in', $session_data);
        
        $data['form'] = 'admin/login/login_form';
        $this->load->view('admin/login/main_login',$data);
    }

    public function register(){
        $data['form'] = 'admin/login/register_form';
        $this->load->view('admin/login/main_login',$data);
    }
    

    
    //San Pham
    
    public function get_san_pham($id=0){
        $this->load->library('pagination');
        $this->load->model('Sanpham_model');
        $this->load->model('Loaisanpham_model');

        $data['loaisanphams'] =  $this->Loaisanpham_model->get_all();
        
        if(empty($id)){
            $data['sanphams'] = $this->Sanpham_model->get_all();
            
        }
        else{
            $data['sanphams'] = $this->Sanpham_model->get_item_with_id($id);
        }
        
        //Pagination
        $total_items  = $this->Sanpham_model->total_items();
        $limit = 3;
        $page = ($this->uri->segment(4)) ? ($this->uri->segment(4) - 1) : 0;
        
        if ($total_items > 0)
        {
            // get current page records
            $data["sanphams"] = $this->Sanpham_model->get_current_page_records($limit, $page*$limit);
                 
            $config['base_url'] = base_url() . 'admin/get_san_pham/page';
            $config['total_rows'] = $total_items;
            $config['per_page'] = $limit;
            $config["uri_segment"] = 4;
             
            // custom paging configuration
            $config['num_links'] = 1;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;
             
            $config['full_tag_open'] = '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">';
            $config['full_tag_close'] = '</ul></nav>';
             
            $config['first_link'] = 'First Page';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
             
            $config['last_link'] = 'Last Page';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';
             
            $config['next_link'] = 'Trang sau';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';
 
            $config['prev_link'] = 'Trang trước';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';
 
            $config['cur_tag_open'] = ' <li class="page-item disabled"><a href="" tabindex="-1">';
            $config['cur_tag_close'] = '</a></li>';
 
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
             
            $this->pagination->initialize($config);
                 
            // build paging links
            $data["links"] = $this->pagination->create_links();
        }
        
        
 
        
        //view
        $data['dynamic_content'] = 'admin/san_pham/main';
		$this->load->view('admin/master',$data);
    }
    
    
    public function get_san_pham_id($id=0){
        $this->load->model('Sanpham_model');
        echo json_encode($this->Sanpham_model->get_item_with_id($id));
    }
    
    public function update_san_pham($id){
        if($this->uploadfile() == false){
            //upload error
            $res  = array('error'=>true,
                          'messenge'=>strip_tags($this->upload->display_errors())
                         );
            if(strip_tags($this->upload->display_errors()) == "You did not select a file to upload."){
                $this->load->model('Sanpham_model');
                $this->Sanpham_model->update($id,1); // update with not choose image
                $res  = array('error'=>false,
                          'messenge'=>'Cập nhật thành công!'
                         );
            }
             echo json_encode($res);
		}
		else
		{
			//upload succeess
            $this->load->model('Sanpham_model');
            
            $sanpham = $this->Sanpham_model->get_item_with_id($id);
            $file = './assets/images/'.$sanpham['hinh_anh'];
            
            $this->Sanpham_model->update($id);
            
            //Xoa anh cu~
             unlink($file);
            
            $res  = array('error'=>false,
                          'messenge'=>'Cập nhật thành công!'
                         );
            
             echo json_encode($res);
		}    
        
       
    }
   
    
    public function create_san_pham(){
        
        if($this->uploadfile() == false){
            //upload error
            $res  = array('error'=>true,
                          'messenge'=>strip_tags($this->upload->display_errors())
                         );
		}
		else
		{
			//upload succeess
            $this->load->model('Sanpham_model');
            $this->Sanpham_model->create();
            
            $res  = array('error'=>false,
                          'messenge'=>'Thêm thành công!'
                         );
            
		}    
        
        echo json_encode($res);
    }
    
    public function delete_san_pham($id){
        $this->load->model('Sanpham_model');
        $sanpham = $this->Sanpham_model->get_item_with_id($id);
        
        $file = './assets/images/'.$sanpham['hinh_anh'];
        
        $effected_row = $this->Sanpham_model->delete($id);
        if($effected_row != 0 ){
            unlink($file);
        }
        echo json_encode($effected_row);
    }
    
    
    
    
    
    public function uploadfile(){
        //upload image
        $config['upload_path'] = './assets/images';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '2048';
		$config['max_width']  = '500';
		$config['max_height']  = '500';
        $config['overwrite'] = TRUE;

		$this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('hinh_anh'))
        {
            return false;
        }
        return  true;

    }
    
    
    public function test(){
        $this->load->library('pagination');

        $config['base_url'] = 'http://example.com/index.php/test/page/';
        $config['total_rows'] = 200;
        $config['per_page'] = 20;

        $this->pagination->initialize($config);

        echo $this->pagination->create_links();
    }

    //Loai San Pham
    public function get_loai_san_pham($id=0)
    {
        # code...
        $this->load->library('pagination');
        $this->load->model('Loaisanpham_model');
        
        if(empty($id)){
            $data['loaisanphams'] = $this->Loaisanpham_model->get_all();
            
        }
        else{
            $data['loaisanphams'] = $this->Loaisanpham_model->get_item_with_id_loai($id);
        }

         //Pagination
         $total_items  = $this->Loaisanpham_model->total_items();
         $limit = 3;
         $page = ($this->uri->segment(4)) ? ($this->uri->segment(4) - 1) : 0;
         
         if ($total_items > 0)
         {
             // get current page records
             $data["loaisanphams"] = $this->Loaisanpham_model->get_current_page_records($limit, $page*$limit);
             $config['base_url'] = base_url() . 'admin/get_loai_san_pham/page';
             $config['total_rows'] = $total_items;
             $config['per_page'] = $limit;
             $config["uri_segment"] = 4;
              
             // custom paging configuration
             $config['num_links'] = 1;
             $config['use_page_numbers'] = TRUE;
             $config['reuse_query_string'] = TRUE;
              
             $config['full_tag_open'] = '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">';
             $config['full_tag_close'] = '</ul></nav>';
              
             $config['first_link'] = 'First Page';
             $config['first_tag_open'] = '<li class="page-item">';
             $config['first_tag_close'] = '</li>';
              
             $config['last_link'] = 'Last Page';
             $config['last_tag_open'] = '<li class="page-item">';
             $config['last_tag_close'] = '</li>';
              
             $config['next_link'] = 'Trang sau';
             $config['next_tag_open'] = '<li class="page-item">';
             $config['next_tag_close'] = '</li>';
  
             $config['prev_link'] = 'Trang trước';
             $config['prev_tag_open'] = '<li class="page-item">';
             $config['prev_tag_close'] = '</li>';
  
             $config['cur_tag_open'] = ' <li class="page-item disabled"><a href="" tabindex="-1">';
             $config['cur_tag_close'] = '</a></li>';
  
             $config['num_tag_open'] = '<li class="page-item">';
             $config['num_tag_close'] = '</li>';
              
             $this->pagination->initialize($config);
                  
             // build paging links
             $data["links"] = $this->pagination->create_links();
         }
         
        //view
        $data['dynamic_content'] = 'admin/loai_san_pham/main';
		$this->load->view('admin/master',$data);
    }

    public function create_loai_san_pham(){
        $this->load->model('Loaisanpham_model');
        $this->Loaisanpham_model->create();
        
        $res  = array('error'=>false,
                        'messenge'=>'Thêm thành công!'
                        );
        echo json_encode($res);
    }

    public function get_loai_san_pham_id($id=0){
        $this->load->model('Loaisanpham_model');
        echo json_encode($this->Loaisanpham_model->get_item_with_id_loai($id));
    }


    public function update_loai_san_pham($id){
            $this->load->model('Loaisanpham_model');
            $this->Loaisanpham_model->update($id);
            $res  = array('error'=>false,
                          'messenge'=>'Cập nhật thành công!'
                         );
            
             echo json_encode($res);
    }    

    public function delete_loai_san_pham($id){
        $this->load->model('Loaisanpham_model');
        
        $effected_row = $this->Loaisanpham_model->delete($id);
        
        echo json_encode($effected_row);
    }


    public function get_user($id=0){
        $this->load->library('pagination');
        $this->load->model('User_model');        

        //Pagination
        $total_items  = $this->User_model->total_items();
        $limit = 3;
        $page = ($this->uri->segment(4)) ? ($this->uri->segment(4) - 1) : 0;
        
        if ($total_items > 0)
        {
            // get current page records
            $data["users"] = $this->User_model->get_current_page_records($limit, $page*$limit);
                 
            $config['base_url'] = base_url() . 'admin/get_user/page';
            $config['total_rows'] = $total_items;
            $config['per_page'] = $limit;
            $config["uri_segment"] = 4;
             
            // custom paging configuration
            $config['num_links'] = 1;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;
             
            $config['full_tag_open'] = '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">';
            $config['full_tag_close'] = '</ul></nav>';
             
            $config['first_link'] = 'First Page';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
             
            $config['last_link'] = 'Last Page';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';
             
            $config['next_link'] = 'Trang sau';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';
 
            $config['prev_link'] = 'Trang trước';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';
 
            $config['cur_tag_open'] = ' <li class="page-item disabled"><a href="" tabindex="-1">';
            $config['cur_tag_close'] = '</a></li>';
 
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
             
            $this->pagination->initialize($config);
                 
            // build paging links
            $data["links"] = $this->pagination->create_links();
        }
        

        //view
        $data['dynamic_content'] = 'admin/user/main';
		$this->load->view('admin/master',$data);
    }

    public function create_user(){
        
			
        $this->load->model('User_model');
        if($this->User_model->registration_insert() == true){
            $res  = array('error'=>false,
            'messenge'=>'Thêm thành công!'
            );
        }
        else{
            $res  = array('error'=>true,
                        'messenge'=> "Email đã được dùng!"
                        );
        }

        echo json_encode($res);
    }


    public function get_user_with_id($id=0){
        $this->load->model('User_model');
        echo json_encode($this->User_model->get_item_with_id($id));
    }


    public function update_user($id){
        $this->load->model('User_model');
        if($this->User_model->update($id) == true){
            $res  = array('error'=>false,
            'messenge'=>'Cập nhật thành công!'
            );
        }
        else{
            $res  = array('error'=>true,
                        'messenge'=> "Lỗi ở  server!"
                        );
        }

        echo json_encode($res);
    } 



    public function delete_user($id){
        $this->load->model('User_model');
        
        $effected_row = $this->User_model->delete($id);
        
        echo json_encode($effected_row);
    }

        
}
