<!--  MODAL THEM SAN PHAM -->
<div class="modal fade" id="modalThemSanPham" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" style="display: none;" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<!--<h5 class="modal-title" id="scrollmodalLabel">Scrolling Long Content Modal</h5>-->
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<div class="card">
										<div class="card-header">
											<strong>Thông Tin</strong> Sản phẩm
										</div>
										<form id='create_san_pham' action="<?php echo base_url().'admin/create_san_pham';?>" method="post"  enctype="multipart/form-data" class="form-horizontal">
										<div class="card-body card-block">
											
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="ten_san_pham" class=" form-control-label">Tên sản phẩm</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="text" id="ten_san_pham" name="ten_san_pham" placeholder="Sản phẩm ..." class="form-control">
													</div>
												</div>
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="select" class=" form-control-label">Loại sản phẩm</label>
													</div>
													<div class="col-12 col-md-9">
														<select name="id_loai" id="select" class="form-control">
															<option value="0">-- Vui lòng chọn loại --</option>
															<?php foreach($loaisanphams as $loaisanpham):?>
																<option value="<?=$loaisanpham->id_loai?>"><?= $loaisanpham->ten_loai;?></option>
															<?php endforeach;?>
														</select>
													</div>
												</div>
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="gia" class=" form-control-label">Giá tiền</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="number" id="gia"  name="gia" placeholder="100.000 " class="form-control">
														<small class="form-text text-muted text-md-right">Đơn vị : VND</small>
													</div>
												</div>
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="mo_ta" class=" form-control-label">Mô tả</label>
													</div>
													<div class="col-12 col-md-9">
														<textarea name="mo_ta" id="mo_ta" rows="9" placeholder="Thông tin chi tiết về sản phẩm.." class="form-control"></textarea>
													</div>
													
												</div>
												

												<div class="row form-group">
													<div class="col col-md-3">
														<label for="hinh_anh" class=" form-control-label">Upload Ảnh</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="file" style="direction:rtl" id="hinh_anh" name="hinh_anh" class="form-control-file hinh_anh">
														<small class="form-text text-muted text-md-right">Chọn ảnh:  .gif | .jpg | .png <br>500x500px <br> Kích thước : nhỏ hơn 2MB </small>
													</div>
													<div class="w-100"></div>
													<div class="col-12 col-md-9">
														<img id="image_preview" width="50%" class="img-fluid image_preview" />
													</div>
												</div>
											
										</div>
										<div class="card-footer">
											<button type="submit"  class="btn btn-primary btn-md float-md-right">
												<i class="fa fa-dot-circle-o"></i> Submit
											</button>
											<button type="reset" class="btn btn-danger btn-md float-md-right mr-md-3">
												<i class="fa fa-ban"></i> Reset
											</button>
										</div>
										</form>
									</div>
								</div>
                            </div>
							</div>
						</div>
						<!--<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn btn-primary">Confirm</button>
						</div>-->
					</div>
				</div>
			</div>
			
			

<!-- END MODAL THEM SAN PHAM -->
<!--  MODAL SUA SAN PHAM -->
<div class="modal fade" id="modalSuaSanPham" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" style="display: none;" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<!--<h5 class="modal-title" id="scrollmodalLabel">Scrolling Long Content Modal</h5>-->
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<div class="card">
										<div class="card-header">
											<strong>Thông Tin</strong> Sản phẩm
										</div>
										<form id='update_san_pham' action="" method="post"  enctype="multipart/form-data" class="form-horizontal">
										<div class="card-body card-block">
											
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="ten_san_pham" class=" form-control-label">Tên sản phẩm</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="text" id="ten_san_pham" name="ten_san_pham" placeholder="Sản phẩm ..." class="form-control">
													</div>
												</div>
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="select" class=" form-control-label">Loại sản phẩm</label>
													</div>
													<div class="col-12 col-md-9">
														<select name="id_loai" id="select" class="form-control">
															<option value="0">-- Vui lòng chọn loại --</option>
															<?php foreach($loaisanphams as $loaisanpham):?>
																<option value="<?=$loaisanpham->id_loai?>"><?= $loaisanpham->ten_loai;?></option>
															<?php endforeach;?>
														</select>
													</div>
												</div>
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="gia" class=" form-control-label">Giá tiền</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="number" id="gia"  name="gia" placeholder="100.000 " class="form-control">
														<small class="form-text text-muted text-md-right">Đơn vị : VND</small>
													</div>
												</div>
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="mo_ta" class=" form-control-label">Mô tả</label>
													</div>
													<div class="col-12 col-md-9">
														<textarea name="mo_ta" id="mo_ta" rows="9" placeholder="Thông tin chi tiết về sản phẩm.." class="form-control"></textarea>
													</div>
												</div>
												

												<div class="row form-group">
													<div class="col col-md-3">
														<label for="hinh_anh" class=" form-control-label">Upload Ảnh</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="file" style="direction:rtl" id="hinh_anh" name="hinh_anh" class="form-control-file hinh_anh">
														<small class="form-text text-muted text-md-right">Chọn ảnh:  .gif | .jpg | .png <br>500x500px <br> Kích thước : nhỏ hơn 2MB </small>
													</div>
													<div class="w-100"></div>
													<div class="col-12 col-md-9">
														<img id="image_preview" width="50%" class="img-fluid image_preview" />
													</div>
												</div>
											
										</div>
										<div class="card-footer">
											<button type="submit"  class="btn btn-primary btn-md float-md-right">
												<i class="fa fa-dot-circle-o"></i> Submit
											</button>
											<button type="reset" class="btn btn-danger btn-md float-md-right mr-md-3">
												<i class="fa fa-ban"></i> Reset
											</button>
										</div>
										</form>
									</div>
								</div>
                            </div>
							</div>
						</div>
						<!--<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn btn-primary">Confirm</button>
						</div>-->
					</div>
				</div>
			</div>
			
			



<!-- END MODAL SUA SAN PHAM -->
<!-- Model Them Loai san pham -->
<div class="modal fade" id="modalThemLoaiSanPham" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" style="display: none;" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<!--<h5 class="modal-title" id="scrollmodalLabel">Scrolling Long Content Modal</h5>-->
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<div class="card">
										<div class="card-header">
											<strong>Thông Tin</strong> Loại sản phẩm
										</div>
										<form id='create_loai_san_pham' action="" method="post"  enctype="multipart/form-data" class="form-horizontal">
										<div class="card-body card-block">
											
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="ten_loai" class=" form-control-label">Tên loại sản phẩm</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="text" id="ten_loai" name="ten_loai" placeholder="Loại sản phẩm ..." class="form-control">
													</div>
												</div>
											
										</div>
										<div class="card-footer">
											<button type="submit"  class="btn btn-primary btn-md float-md-right">
												<i class="fa fa-dot-circle-o"></i> Submit
											</button>
											<button type="reset" class="btn btn-danger btn-md float-md-right mr-md-3">
												<i class="fa fa-ban"></i> Reset
											</button>
										</div>
										</form>
									</div>
								</div>
                            </div>
							</div>
						</div>
						<!--<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn btn-primary">Confirm</button>
						</div>-->
					</div>
				</div>
			</div>
			
			


<!--END Model Them Loai san pham-->
<!-- Model Them Loai san pham-->
<div class="modal fade" id="modalSuaLoaiSanPham" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" style="display: none;" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<!--<h5 class="modal-title" id="scrollmodalLabel">Scrolling Long Content Modal</h5>-->
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<div class="card">
										<div class="card-header">
											<strong>Thông Tin</strong> Loại sản phẩm
										</div>
										<form id='update_loai_san_pham' action="" method="post"  enctype="multipart/form-data" class="form-horizontal">
										<div class="card-body card-block">
											
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="ten_loai" class=" form-control-label">Tên loại sản phẩm</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="text" id="ten_loai" name="ten_loai" placeholder="Loại sản phẩm ..." class="form-control">
													</div>
												</div>
											
										</div>
										<div class="card-footer">
											<button type="submit"  class="btn btn-primary btn-md float-md-right">
												<i class="fa fa-dot-circle-o"></i> Submit
											</button>
											<button type="reset" class="btn btn-danger btn-md float-md-right mr-md-3">
												<i class="fa fa-ban"></i> Reset
											</button>
										</div>
										</form>
									</div>
								</div>
                            </div>
							</div>
						</div>
						<!--<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn btn-primary">Confirm</button>
						</div>-->
					</div>
				</div>
			</div>
			
<!--END Model Sua Loai san pham-->




<!--  MODAL THEM USER -->
<div class="modal fade" id="modalThemUser" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" style="display: none;" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<!--<h5 class="modal-title" id="scrollmodalLabel">Scrolling Long Content Modal</h5>-->
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<div class="card">
										<div class="card-header">
											<strong>Thông Tin</strong> User
										</div>
										<form id='create_user_form' action="" method="post"  enctype="multipart/form-data" class="form-horizontal">
										<div class="card-body card-block">
											
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="username" class=" form-control-label">Username</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="text" id="username" name="username" placeholder="Username ..." class="form-control">
													</div>
												</div>
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="role" class=" form-control-label">Role</label>
													</div>
													<div class="col-12 col-md-9">
														<select name="role" id="role" class="form-control">
															<option value="-1">-- Vui lòng chọn vai trò --</option>
																<option value="0">Member</option>
																<option value="1">Admin</option>
														</select>
													</div>
												</div>
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="email" class=" form-control-label">Email Input</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="email" id="email" name="email" placeholder="Enter Email" class="form-control">
														
													</div>
                                           		</div>
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="password" class=" form-control-label">Password</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="password" id="password" name="password" placeholder="Password" class="form-control">
														
													</div>
                                            	</div>
												
											
										</div>
										<div class="card-footer">
											<button type="submit"  class="btn btn-primary btn-md float-md-right">
												<i class="fa fa-dot-circle-o"></i> Submit
											</button>
											<button type="reset" class="btn btn-danger btn-md float-md-right mr-md-3">
												<i class="fa fa-ban"></i> Reset
											</button>
										</div>
										</form>
									</div>
								</div>
                            </div>
							</div>
						</div>
						<!--<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn btn-primary">Confirm</button>
						</div>-->
					</div>
				</div>
			</div>
			
			

<!-- END MODAL THEM USER -->
<!--  MODAL SUA USER -->
<div class="modal fade" id="modalSuaUser" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" style="display: none;" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<!--<h5 class="modal-title" id="scrollmodalLabel">Scrolling Long Content Modal</h5>-->
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<div class="card">
										<div class="card-header">
											<strong>Thông Tin</strong> User
										</div>
										<form id='update_user_form' action="" method="post"  enctype="multipart/form-data" class="form-horizontal">
										<div class="card-body card-block">
											
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="username" class=" form-control-label">Username</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="text" id="username" name="username" placeholder="Username ..." class="form-control">
													</div>
												</div>
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="role" class=" form-control-label">Role</label>
													</div>
													<div class="col-12 col-md-9">
														<select name="role" id="role" class="form-control">
															<option value="-1">-- Vui lòng chọn vai trò --</option>
																<option value="0">Member</option>
																<option value="1">Admin</option>
														</select>
													</div>
												</div>
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="email" class=" form-control-label">Email Input</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="email" id="email" name="email" placeholder="Enter Email" class="form-control">
														
													</div>
                                           		</div>
												<div class="row form-group">
													<div class="col col-md-3">
														<label for="password" class=" form-control-label">Password</label>
													</div>
													<div class="col-12 col-md-9">
														<input type="password" id="password" name="password" placeholder="Password" class="form-control">
														
													</div>
                                            	</div>
												
											
										</div>
										<div class="card-footer">
											<button type="submit"  class="btn btn-primary btn-md float-md-right">
												<i class="fa fa-dot-circle-o"></i> Submit
											</button>
											<button type="reset" class="btn btn-danger btn-md float-md-right mr-md-3">
												<i class="fa fa-ban"></i> Reset
											</button>
										</div>
										</form>
									</div>
								</div>
                            </div>
							</div>
						</div>
						<!--<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn btn-primary">Confirm</button>
						</div>-->
					</div>
				</div>
			</div>
			


<!-- END MODAL SUA USER -->



