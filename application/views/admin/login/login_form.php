<div class="login-form">
                                <?php if(isset($mess_error)):?>
                                <div class="form-group">
                                    <div class="alert alert-danger" role="alert">
                                        <?= $mess_error;?>
                                    </div> 
                                </div>
                                <?php endif;?>

                                <?php if(isset($vali_error)):?>
                                <div class="form-group">
                                    <div class="alert alert-danger" role="alert">
                                        <?= $vali_error;?>
                                    </div> 
                                </div>
                                <?php endif;?>
                                
                            <form action="<?php echo base_url();?>UserAuthentication/process_login" method="post">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input class="au-input au-input--full" type="email" name="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="au-input au-input--full" type="password" name="password" placeholder="Password">
                                </div>
                                
                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>
                                <!--<div class="social-login-content">
                                    <div class="social-button">
                                        <button class="au-btn au-btn--block au-btn--blue m-b-20">sign in with facebook</button>
                                        <button class="au-btn au-btn--block au-btn--blue2">sign in with twitter</button>
                                    </div>
                                </div>-->
                            </form>
                            <div class="register-link">
                                <p>
                                    Don't you have account?
                                    <a href="<?php echo base_url()?>admin/register">Sign Up Here</a>
                                </p>
                            </div>
                        </div>