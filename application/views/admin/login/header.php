
<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('admin/head');?>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <img src="<?php echo base_url();?>assets/images/icon/logo.png" alt="CoolAdmin">
                            </a>
                        </div>