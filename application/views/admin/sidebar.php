<aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="<?php echo base_url()?>assets/#">
                    <img src="<?php echo base_url()?>assets/images/icon/logo.png" alt="Cool Admin" />
                </a>
            </div>
            
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="has-sub">
                            <a class="js-arrow" href="<?php echo base_url()?>assets/#">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?php echo base_url()?>assets/index.html">Dashboard 1</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/index2.html">Dashboard 2</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/index3.html">Dashboard 3</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/index4.html">Dashboard 4</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>admin/get_user">
                                <i class="fas fa-chart-bar"></i>Acounts</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>admin/get_san_pham">
                                <i class="fas fa-table"></i>Sản Phẩm</a>
                        </li>

                        <li>
                            <a href="<?php echo base_url()?>admin/get_loai_san_pham">
                                <i class="fas fa-table"></i>Loại sản Phẩm</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>assets/form.html">
                                <i class="far fa-check-square"></i>Forms</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>assets/#">
                                <i class="fas fa-calendar-alt"></i>Calendar</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url()?>assets/map.html">
                                <i class="fas fa-map-marker-alt"></i>Maps</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="<?php echo base_url()?>assets/#">
                                <i class="fas fa-copy"></i>Pages</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?php echo base_url()?>assets/login.html">Login</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/register.html">Register</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/forget-pass.html">Forget Password</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="<?php echo base_url()?>assets/#">
                                <i class="fas fa-desktop"></i>UI Elements</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?php echo base_url()?>assets/button.html">Button</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/badge.html">Badges</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/tab.html">Tabs</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/card.html">Cards</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/alert.html">Alerts</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/progress-bar.html">Progress Bars</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/modal.html">Modals</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/switch.html">Switchs</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/grid.html">Grids</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/fontawesome.html">Fontawesome Icon</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?>assets/typo.html">Typography</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    
                </nav>
            </div>
            
        </aside>