 <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">Danh sách loại sản phẩm</h3>
                                <div class="table-data__tool">
                                   
                                    <div class="table-data__tool-left"></div>
                                    <div class="table-data__tool-right">
                                        <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#modalThemLoaiSanPham">
                                            <i class="zmdi zmdi-plus"></i>Thêm</button>
                                        
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </th>
                                                <th>Tên loại sản phẩm</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(isset($loaisanphams)):?>
                                            <?php foreach($loaisanphams as $loaisanpham):?>
                                                <tr class="tr-shadow">
                                                    <td>
                                                        <label class="au-checkbox">
                                                            <input type="checkbox">
                                                            <span class="au-checkmark"></span>
                                                        </label>
                                                    </td>
                                                    <td><?= $loaisanpham->ten_loai;?></td>

                                                    <td>
                                                        <div class="table-data-feature">
                                                            <button class="item"  data-idsanpham="<?= $loaisanpham->id_loai;?>" data-toggle="modal" data-target="#modalSuaLoaiSanPham" data-placement="top" title="Sửa">
                                                                <i class="zmdi zmdi-edit"></i>
                                                            </button>
                                                            <button class="item delete_sanpham" data-idsanpham="<?= $loaisanpham->id_loai;?>"  data-toggle="tooltip" data-placement="top" title="Xóa">
                                                                <i class="zmdi zmdi-delete"></i>
                                                            </button>
                                                            <button class="item" data-toggle="tooltip" data-placement="top" title="Chi tiết">
                                                                <i class="zmdi zmdi-more"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="spacer"></tr>
                                            <?php endforeach; ?>
                                         <?php endif;?>  
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
</div>



<div class="row">
    <div class="col-md-12">
        <?php if(isset($links)):?>
            <?php echo $links;?>
        <?php endif;?>
    </div>
</div>
