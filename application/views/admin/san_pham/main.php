 <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">Danh sách sản Phẩm</h3>
                                <div class="table-data__tool">
                                    <!--<div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            <select class="js-select2" name="property">
                                                <option selected="selected">All Properties</option>
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <div class="rs-select2--light rs-select2--sm">
                                            <select class="js-select2" name="time">
                                                <option selected="selected">Today</option>
                                                <option value="">3 Days</option>
                                                <option value="">1 Week</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <button class="au-btn-filter">
                                            <i class="zmdi zmdi-filter-list"></i>filters</button>
                                    </div>-->
                                    <div class="table-data__tool-left"></div>
                                    <div class="table-data__tool-right">
                                        <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#modalThemSanPham">
                                            <i class="zmdi zmdi-plus"></i>Thêm</button>
                                        
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </th>
                                                <th>Tên sản phẩm</th>
                                                <th>Ảnh</th>
                                                <th>Loại</th>
                                                <th>Mô tả</th>
                                                
                                                <th>Giá</th>
                                                <th>Tác Vụ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(isset($sanphams)):?>
                                            <?php foreach($sanphams as $sanpham):?>
                                                <tr class="tr-shadow">
                                                    <td>
                                                        <label class="au-checkbox">
                                                            <input type="checkbox">
                                                            <span class="au-checkmark"></span>
                                                        </label>
                                                    </td>
                                                    <td><?= $sanpham->ten_san_pham;?></td>
                                                    <td>
                                                        <img class="img-fluid my-img"  src="<?= base_url().'assets/images/'.$sanpham->hinh_anh;?>"/>
                                                    </td>
                                                    <td class="desc"><?php 
                                                        foreach($loaisanphams as $loaisanpham){
                                                            if($loaisanpham->id_loai == $sanpham->id_loai) {
                                                                echo $loaisanpham->ten_loai;
                                                                break;
                                                            }
                                                        }
                                                    ?></td>
                                                    <td><?= $sanpham->mo_ta;?></td>
                                                    <td><?= $sanpham->gia;?></td>
                                                    <td>
                                                        <div class="table-data-feature">
                                                            <button class="item"  data-idsanpham="<?= $sanpham->id;?>" data-toggle="modal" data-target="#modalSuaSanPham" data-placement="top" title="Sửa">
                                                                <i class="zmdi zmdi-edit"></i>
                                                            </button>
                                                            <button class="item delete_sanpham" data-idsanpham="<?= $sanpham->id;?>"  data-toggle="tooltip" data-placement="top" title="Xóa">
                                                                <i class="zmdi zmdi-delete"></i>
                                                            </button>
                                                            <button class="item" data-toggle="tooltip" data-placement="top" title="Chi tiết">
                                                                <i class="zmdi zmdi-more"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="spacer"></tr>
                                            <?php endforeach; ?>
                                         <?php endif;?>  
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
</div>



<div class="row">
    <div class="col-md-12">
        <?php if(isset($links)):?>
            <?php echo $links;?>
        <?php endif;?>
    </div>
</div>
