<?php
class User_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all(){
        $query = $this->db->get('users');
        return $query->result();
    } 


    function get_item_with_id($id){
        $query = $this->db->where('id',$id)->get('users');
        return $query->row_array();
    }

    function update($id){
        $data = $this->input->post();
        $data['password'] = MD5($data['password']);
            // Query to insert data in database
        $this->db->update('users', $data, array('id'=>$id));
        if ($this->db->affected_rows() > 0) 
            return true;
        else 
            return false;

    }


    function delete($id){
        $this->db->delete('users', array('id'=>$id));
        return $this->db->affected_rows();
    }



    function total_items(){
        $query = $this->db->get('users');
        return $query->num_rows();
    }

    function get_current_page_records($limit, $start){
        $this->db->limit($limit, $start);
        $query = $this->db->get("users");

        if ($query->num_rows() > 0)
            return $query->result();

        return false;
    }


    public function login() {
        $data = $this->input->post();

        $condition = "email =" . "'" . $data['email'] . "' AND " . "password =" . "'" . MD5($data['password']) . "'";
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        
        if ($query->num_rows() == 1) 
            return true;
        else 
            return false;
        
    }

    // Read data from database to show data in admin page
    public function read_user_information($email) {

        $condition = "email =" . "'" . $email . "'";
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        
        if ($query->num_rows() == 1) 
            return $query->row_array();
        else 
            return false;
        
    }


    // Insert registration data in database
    public function registration_insert() {

        $data = $this->input->post();
        $data['password'] = MD5($data['password']);
        // Query to check whether username already exist or not
        $condition = "email =" . "'" . $data['email'] . "'";
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            // Query to insert data in database
            $this->db->insert('users', $data);
            if ($this->db->affected_rows() > 0) 
                return true;
        }else 
            return false;
        
    }
    
    
}
