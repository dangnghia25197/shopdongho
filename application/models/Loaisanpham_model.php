<?php
class Loaisanpham_model extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function total_items(){
        $query = $this->db->get('loai_san_pham');
        return $query->num_rows();
    }
    function get_all(){
        $query = $this->db->get('loai_san_pham');
        return $query->result();
    }
    
    function get_item_with_id_loai($id_loai){
        $query = $this->db->where('id_loai',$id_loai)->get('loai_san_pham');
        return $query->row_array();
    }
    
    function create(){
        $data = $this->input->post();
    
        $this->db->insert('loai_san_pham', $data);
        
    }
    
    function update($id_loai,$op = 0){
        $data = $this->input->post();
        
        $this->db->update('loai_san_pham', $data, array('id_loai'=>$id_loai));
    }
    
    function delete($id_loai){
        $this->load->model('Sanpham_model');
        $this->db->delete('loai_san_pham', array('id_loai'=>$id_loai));
        if($this->db->affected_rows() !=0 ){
            $res = $this->Sanpham_model->deleteallwithidloai($id_loai);
        }
        return $this->db->affected_rows();
    }
    
    
    function get_current_page_records($limit, $start){
        $this->db->limit($limit, $start);
        $query = $this->db->get("loai_san_pham");

        if ($query->num_rows() > 0)
            return $query->result();

        return false;
    }
    
}
