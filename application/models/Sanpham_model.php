<?php
class Sanpham_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function total_items(){
        $query = $this->db->get('san_pham');
        return $query->num_rows();
    }
    function get_all(){
        $query = $this->db->get('san_pham');
        return $query->result();
    }
    
    function get_item_with_id($id){
        $query = $this->db->where('id',$id)->get('san_pham');
        return $query->row_array();
    }
    
    function create(){
        $data = array(
        'id' => NULL,
        'id_loai' => $_POST['id_loai'],
        'ten_san_pham' => $_POST['ten_san_pham'],
        'mo_ta' => $_POST['mo_ta'],
         'gia'  =>  $_POST['gia'],
        'hinh_anh' => $this->upload->data('file_name'));
            
        $this->db->insert('san_pham', $data);
        
    }
    
    function update($id,$op = 0){
         $data = array(
        'id_loai' => $_POST['id_loai'],
        'ten_san_pham' => $_POST['ten_san_pham'],
        'mo_ta' => $_POST['mo_ta'],
         'gia'  =>  $_POST['gia']
        );
        if($op == 0)  /// Neu admin muon upload 1 anh khac
            $data['hinh_anh'] = $this->upload->data('file_name');
        
        $this->db->update('san_pham', $data, array('id'=>$id));
    }
    
    function delete($id){
        $this->db->delete('san_pham', array('id'=>$id));
        return $this->db->affected_rows();
    }

    function deleteallwithidloai($idloai){
        $this->db->delete('san_pham', array('id_loai'=>$idloai));
        return $this->db->affected_rows();
    }
    
    
    function get_current_page_records($limit, $start){
        $this->db->limit($limit, $start);
        $query = $this->db->get("san_pham");

        if ($query->num_rows() > 0)
            return $query->result();

        return false;
    }
    
    
    
    
//    function get_last_ten_entries()
//    {
//        $query = $this->db->get('entries', 10);
//        return $query->result();
//    }
//
//    function insert_entry()
//    {
//        $this->title   = $_POST['title']; // please read the below note
//        $this->content = $_POST['content'];
//        $this->date    = time();
//
//        $this->db->insert('entries', $this);
//    }
//
//    function update_entry()
//    {
//        $this->title   = $_POST['title'];
//        $this->content = $_POST['content'];
//        $this->date    = time();
//
//        $this->db->update('entries', $this, array('id' => $_POST['id']));
//    }
}
